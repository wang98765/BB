FROM superbigfu/alpine-jdk:1.8.0_111
#FROM openjdk:8u191-jre-alpine3.9
#FROM openjdk:8-jre-alpine

# 镜像维护者姓名或邮箱地址
MAINTAINER 125043150@qq.com

WORKDIR /bb-project
#设置变量 1=关闭，其他的输出
ENV outLogClose=""

#打包变量
ARG PROJECT_NAME

#COPY ./target/${PROJECT_NAME}-release/${PROJECT_NAME} /project
ADD target/${PROJECT_NAME}-release.tar.gz /

#RUN mkdir /bb-project/log
RUN mkdir -p /bb-project/log && \
    ln -s /bb-project/log /logs


#jfinal.sh start 4 /opt/logs/
#CMD ["java", "-version"]
#ENTRYPOINT ["sh","jfinal.sh","start",${outLogClose}]
CMD sh jfinal.sh start 4 /bb-project/log
#ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS $ENV_OPTS $REMOTE2 -jar ko-assy-db.jar" ]

EXPOSE 801
VOLUME /logs

#启动
#docker run -d --name bb-project -v /本地路径:/logs -e outLogClose="1" -p 本地端口:801 registry.cn-hangzhou.aliyuncs.com/zhao_docks/bb-project:1.0
#docker run -d --name bb-project -v /opt/logs:/logs -e outLogClose="1" -p 801:801 registry.cn-hangzhou.aliyuncs.com/zhao_docks/bb-project:1.0

#docker run -d --name bb-project-setup -p 801:801 -v /opt/app_logs:/logs -e outLogClose="2" registry.cn-hangzhou.aliyuncs.com/zhao_docks/bb-project:1.0.1-setup
