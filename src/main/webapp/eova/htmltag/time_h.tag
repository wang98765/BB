<%
//debug(f);
//debug(value);
// data-options
var verify = "";
if(isTrue(isNoN!) ){
	verify = "required=true";
}
if(!isEmpty(validator!)){
	if(verify != ''){
		verify += ';';
	}
	verify = verify + validator!;
}



if(!isEmpty(value!)){
	
	if(!strutil.contain(value!+'','$')){
		value = strutil.formatDate(value!, options!);
	}
}
var disabled="";
if(isTrue(isReadonly!) ||isTrue(view!)){
	disabled = 'readonly';
	verify = '';
}
//options =yyyy-MM-dd hh:mm:ss 要么  yyyy-MM-dd 控件要求格式：yyyy-mm-dd hh:ii:ss  hh才是24小时制

value = isEmpty( value! ) ? defaultValue!:value!;
if(!isEmpty(value!) && options =='HH:mm:ss' && strutil.length(value!) == 8){//只有时分秒的情况下，额外补年月日
	//value = '2021-07-26 '+value!;
	var now = date();
	//value = strutil.formatDate(now, 'yyyy-MM-dd')+" "+value!;
}
%>
<div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control" autocomplete="off" id="${id!}" name="${name}" placeholder="${placeholder!}" bb-verify="${verify!}" ${strutil.replace(verify!,';',' ')} ${disabled!} autocomplete="off" >
</div>
<script>
$(function(){
	<!-- datetimepicker示例 -->
	var format = '${options}'.replace('mm','ii').replace('MM','mm').replace('HH','hh');

	var minView = 'day';
	var startView ='month';
	if('${options}' == 'yyyy-MM-dd'){//yyyy-MM-dd 则选择器只有年月日选择
		minView = 'month';
	}else if('${options}' == 'HH:mm:ss'){
		minView = 'month';
		startView='hour';
	}
	//console.log('${name}'+":"+format);
	<% if(!isTrue(isReadonly!) && !isTrue(view!)){//只读模式不会触发时间选择  %>  
	$('#${id!}').datetimepicker({
			format: format,
		    minView: minView,   //
		    autoclose: true,
		    todayBtn:  true,
			startView:startView
    });
	<% } %>
	<% if(!isEmpty(value!)){ %>
		$("#${id!}").val('${value!}');
	<% } %>
});
</script>