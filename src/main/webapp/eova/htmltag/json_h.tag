<%
    // data-options


    var verify = "";
    if(isTrue(isNoN!) ){
    verify = "required=true";
    }
    if(!isEmpty(validator!)){
    if(verify != ''){
    verify += ';';
    }
    verify = verify + validator!;
    }



    if( isTrue(isReadonly!) || isTrue(view!)  ){
    verify ='';
    }

    if(!isEmpty(width!)){
    if(!strutil.contain(width!,'%') && !strutil.contain(width!,'px')){
    width = width+'%';
    }
    }
    debug(height!);
    if(!isEmpty(height!)){
    if(!strutil.contain(height!,'%') && !strutil.contain(height!,'px')){
    height = height+'px';
    }
    }else
    height =80+'px';


    value = isEmpty( value! ) ? defaultValue!:value!;


%>
<div class="bb_jsonedit">
    <!-- <div id="jsonResult"  style="width:100%;height:450px;"></div> -->
    <div id="${id!}_sqleditor" style="width:100%;height:${height!};"> </div>


    <textarea style="display:none" id="${id!}" name="${name!}" bb-verify="${verify!}" placeholder="${placeholder!}" ${strutil.replace(verify!,';',' ')} target='${id!}_sqleditor'>${value!}</textarea>
</div>
<script type="text/javascript">
    $(function(){

        //设置值var jsValue = jseditor.getValue(); jseditor.setValue(codeopt.opt.content);

        var readOnly = false;
        <% if( isTrue(isReadonly!) || isTrue(view!)  ){ %>
        readOnly = true;
        <% }%>
        var jseditor = initJsEditor('${id!}_sqleditor');


        var datason = $('#${id!}').val();
        jseditor.setValue(datason);

        //if(datason != '') {
        //    datason = JSON.parse(datason);
        //}
        $('#${id!}').attr('value', datason);

        if(!readOnly){
            jseditor.getSession().on ('change', function (e) {
                //$('#${id!}').attr('value', jseditor.getValue());
                $("#${id!}").val(jseditor.getValue());
            });
        }

        //初始化代码编辑器
        function initJsEditor(id,readOnly){
            //获取控件   id ：codeEditor
            let editor = ace.edit(id);
            //设置风格和语言（更多风格和语言，请到github上相应目录查看）
            theme = "monokai";
            theme = "terminal";

            theme = "monokai";
            //语言
            language = "json";
            //editor.setTheme("ace/theme/" + theme);
            editor.session.setMode("ace/mode/" + language);
            //字体大小
            editor.setFontSize(15);
            //设置只读（true时只读，用于展示代码）
            editor.setReadOnly(false);
            //自动换行,设置为off关闭
            editor.setOption("wrap", "free");
            //启用提示菜单
            ace.require("ace/ext/language_tools");
            editor.setOptions({
                enableBasicAutocompletion: true,
                enableSnippets: true,
                enableLiveAutocompletion: true,
                autoScrollEditorIntoView: true
            });

            if(readOnly){
                editor.setReadOnly(true);  // false to make it editable
            }

            return editor;
        }

    });
</script>
