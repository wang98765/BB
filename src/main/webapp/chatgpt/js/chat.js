/**
 * 同步聊天（主要是测试用）
 * @param inputId
 * @param sessionId
 */
function chat(inputId,sessionId){
    let inputTxt = $("#"+inputId).val();
    if(inputTxt != ''){
        let data = {};
        data.sessionId= sessionId;
        data.question= inputTxt;

        JSON.stringify(data.data);

        let now = new Date();
        $.ajax({
            type: "POST",
            url: "/aiChat/gptChat",
            dataType: "json",
            data: data,
            beforeSend: function() {
                let sent = {
                    id: '',
                    type: 'sent',
                    message: inputTxt,
                    time: $.formatDateTime(now, 'hh:mm:ss A'),
                }
                pushUiOneMsg(sent,false);

                //重置输入框
                $("#"+inputId).val("");
            },
            success: function (resp) {
                let respMessage = resp.data.finalContent;

                let received = {
                    id: '',
                    type: 'received',
                    message: respMessage,
                    time: $.formatDateTime(new Date(), 'hh:mm:ss A'),
                }
                pushUiOneMsg(received,false);
            },
            error: function (err) {
                //先提示吧，后期可以吧消息发到上面但是 错误提示信息页放上去
                data.error(err)
            }
        })

    }
}

var websocket = null;
/**
 * 目标状态：1=要上，0=不需要上
 * @type {number}
 */
var targetState = 0;
/**
 * 发送的消息放入消息框的情况：1=发送成功，2=发了就推上去 （仅仅针对异步的）
 * @type {number}
 */
var chatSendType = 2;
/**
 * 响应式聊天
 * @param inputId
 * @param sessionId
 */
function steamchat(inputId,sessionId,token,host){
    let inputTxt = $("#"+inputId).val();
    if(inputTxt != ''){

        if(websocket == null){
            websocket = startws("ws://"+host+"/aichat.ws/"+token);
            if(websocket == null){
                layer.alert("当前浏览器 Not support websocket", {icon: 2});
                return;
            }
        }
        targetState = 1;

        let data = {};
        data.sessionId= sessionId;
        data.question= inputTxt;

        JSON.stringify(data.data);
        $.ajax({
            type: "POST",
            url: "/aiChat/gptChatStream",
            dataType: "json",
            data: data,
            beforeSend: function() {
                if(chatSendType == 2){
                    let time =  (new Date()).getTime();
                    pushSendMsg2window(time,inputTxt,time,inputId);
                }
            },
            success: function (resp){
                let time = resp.time;

                if (chatSendType == 1){
                    pushSendMsg2window(time,inputTxt,time,inputId);
                }
            },
            error: function (err) {
                //先提示吧，后期可以吧消息发到上面但是 错误提示信息页放上去
                data.error(err)
            }
        });
    }
}

function  pushSendMsg2window(time,sendTxt,reveiveId,inputId){
    let sent = {
        id: 's'+time.toString,
        type: 'sent',
        message: sendTxt,
        time: $.formatDateTime(new Date(time), 'hh:mm:ss A'),
    }
    pushUiOneMsg(sent,false);

    //重置输入框
    $("#"+inputId).val("");

    lastRecieveMsgId = reveiveId;
    //放一条等待中消息，等待后面的ws接收
    let loading = template('loading-message', {});
    let respMessage = loading;

    let received = {
        id: lastRecieveMsgId,
        type: 'received',
        message: respMessage,
        time: $.formatDateTime(new Date(time), 'hh:mm:ss A'),
    }
    pushUiOneMsg(received,false);
}

/**
 * 连接状态：0=未连接（无需重连），1=连接成功,2=掉线（需要重连）
 * @type {number}
 */
var robotState = 0;
function robotStateChanged(state) {
    robotState = state;
    //online/offline
    if(robotState == 1){
        console.log("上线啦~！");
        $("#robot_"+robotId+"_state").removeClass("offline");
        $("#robot_"+robotId+"_state").addClass("online");
    }else{
        console.log("下线啦~！");
        $("#robot_"+robotId+"_state").removeClass("online");
        $("#robot_"+robotId+"_state").addClass('offline');
    }

    if(state == 2 && targetState == 1){//尝试启动一次
        startws(websocketUrl);
    }


}

//发完消息后会设置此值
var lastRecieveMsgId = null;
/**
 * 收到ws消息
 * @param msg
 */
function messageReceived(msg) {
    log.log(msg);
    //把接收到消息添加到lastRecieveMsgId上
    if(lastRecieveMsgId != null) {
        let msgObj = JSON.parse(msg);//{"role":"assistant","content":"[DONE]","time":1694162275917}
        let content = msgObj.content;
        if ( content!='[DONE]') {

            $("#msg_"+lastRecieveMsgId+"_content").append(content);
            scrollElementToEnd("msg_"+lastRecieveMsgId+"_time");
        }else{
            //删除加载中
            $(".chatloading").each(function(){
                $(this).remove();
            });
            lastRecieveMsgId = null;
            //接收框改成已完成，，暂时未知设置
        }
    }
}



function closeWebSocket(){
    targetState = 0;
    if(websocket != null)
        websocket.close();
}

var websocketUrl = null;
function startws(url){
    if ('WebSocket' in window) {
        //websocket = new WebSocket("ws://127.0.0.1:801/aiChatWs");
        //websocket = new WebSocket("ws://127.0.0.1:801/aichat.ws/59ded1f906544508a6167d8ff3b124c2");
        websocket = new WebSocket(url);

        //连接成功建立的回调方法
        websocket.onopen = function () {
            log.info("WebSocket连接成功!");
            robotStateChanged(1);
        }

        websocketUrl = url;

        //连接发生错误的回调方法
        websocket.onerror = function () {
            log.error("WebSocket连接发生错误");//需要重连么？
            robotStateChanged(0);
        };

        //连接关闭的回调方法
        websocket.onclose = function () {
            log.info("WebSocket连接关闭!");//启动重连三次
            robotStateChanged(2);
        }

        //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
        window.onbeforeunload = function () {
            closeWebSocket();
        }

        websocket.onmessage = function (event) {
            messageReceived(event.data);
        }

        return websocket;
    }
    else {
        //alert('当前浏览器 Not support websocket');
        log.info("当前浏览器 Not support websocket");
        return null;
    }
}

/**
 * 初始当前会话的历史消息
 * @param chartSession
 */
function initHisMessage(chartSession) {
    //let sessionId = chartSession.sessionId;

    $.ajax({
        url: '/aiChat/getCurrentSesionRecords?limit=100',
        dataType: 'json',
        success: function(data) {
            // 将返回数据转换为JSON对象
            console.log(data);
            //先清空
            $('#messages').append('');
            //id,type(sent/received),message、time(07:15)
            for (let i = 0; i < data.data.length; i++) {
                let record = data.data[i];
                var line = {
                    id: record.id,
                    type: record.role =='user'?'sent':'received',
                    message: record.content,
                    time: $.formatDateTime(new Date(record.create_time), 'hh:mm:ss A'),
                }
                let str = template('tpl-message', line);
                //log.log(str)

                pushUiOneMsg(line,false);

                $('#current_robot_lastTime').html("last seen at "+$.formatDateTime(new Date(record.create_time), 'hh:mm A'));

            }
        }
    });
}

/**
 * 初始左侧机器人列表（目前只有一个）
 * @param chartSession
 */
function initRobots(chartSession){
    //定义模板数据
    var data = {
        robotId: chartSession.robot.id,
        robotName: chartSession.robot.name,
        robotAvatar: chartSession.robot.avatar,
        robotSessionTime: chartSession.aiChatSession.create_time,
    }
    let str = template('tpl-robot', data);

    $('#robot_list').html(str);

    initCurrentRobot(chartSession);
}

/**
 *  当前机器人ID
 * @type {null}
 */
var robotId = null;
function initCurrentRobot(chartSession){
    robotId = chartSession.robot.id;
    log.info("robotId="+robotId);
    //设置当前机器人
    $("#current_robot_avatar").attr("src", chartSession.robot.avatar);
    $('#current_robot_name').html(chartSession.robot.name);
    $('#current_robot_lastTime').html("last seen at "+$.formatDateTime(new Date(chartSession.aiChatSession.create_time), 'hh:mm A'));

    $("#robot_"+robotId+"_msgnun").text(0);

    initHisMessage(chartSession);
}


/**
 * 推送一条消息到界面
 * line格式：
 * {
 *                     id: record.id,
 *                     type: record.role =='user'?'sent':'received',
 *                     message: record.content,
 *                     time: $.formatDateTime(new Date(record.create_time), 'hh:mm:ss A'), 格式化过的
 *                     failReason: "xxx"    //  仅当发送的消息且失败了返回
 *                 }
 * @param line
 * @param needClean
 */
function pushUiOneMsg(line,needClean){
    console.log(template)
    //template.defaults.minimize=true;
    //template.config("escape", false);
    let str = template('tpl-message', line);
    log.log(str)

    if(needClean){
        $('#messages').html(str);
        $("#robot_"+robotId+"_msgnun").text(1);
    }else{
        $('#messages').append(str);
        $("#robot_"+robotId+"_msgnun").text(Number($("#robot_"+robotId+"_msgnun").text()) + 1);
    }

    $('#current_robot_lastTime').html("last seen at "+line.time);



    //msg_{{id}}_time
    scrollElementToEnd();
}


/**
 *  最后个对话窗口到最底部可见
 * @param id
 */
function scrollElementToEnd(id){
    //const lastTime = document.getElementById( id );
   // if(lastTime != null){
        //lastTime.scrollIntoView({behavior: "instant", block: "end", inline: "nearest"});

        document.getElementById( "messages").scrollIntoView({behavior: "instant", block: "end", inline: "nearest"});
        // lastTime.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
    //}
}

