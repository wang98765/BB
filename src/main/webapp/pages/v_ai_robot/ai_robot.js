/**
 * 开通微信群（开通是异步的）
 * @param robotId
 */
function openWxGroup(robotId){
    $.ajax({
        type: "POST",
        url: "/qywxGroup/sbumitQywx?robotId="+robotId,
        success: function (resp) {
            let robotInfo = resp.data;
            let qywx_state = robotInfo.qywx_state;

            //微信客服状态：0=初始,1=ok,2=处理中
            if(qywx_state == 1){
                layer.msg('已开通！', {icon: 1});
                reload();
            }else if(qywx_state == 2){

                layer.alert('开通中，请稍后刷新！！', { icon: 1, title: '提示' },function(index){
                    //点击确定后再执行的语句
                    layer.close(index);
                    reload();
                });

            }else{
                layer.msg('开通群失败，请稍后再试', {icon: 5}, {anim:6});
            }
        }
    })

}

/**
 * 提取qrcode（确保状态qywx_state == 1）
 */
function getWxGroupQrCode(robotId){
    $.ajax({
        type: "POST",
        url: "/qywxGroup/getQywxQrcode?robotId="+robotId,
        success: function (resp) {
            let robotInfo = resp.data;
            let qywx_state = robotInfo.qywx_state;
            let qywx_qr_code = robotInfo.qywx_qr_code;

            //微信客服状态：0=初始,1=ok,2=处理中
            if(qywx_state == 1 && qywx_qr_code != ''){
                layer.msg('已开通！', {icon: 1});

                //显示图片
                //$.table.imageView(qywx_qr_code,'450');//高度
                $.tableLineImgShow(qywx_qr_code,400,400);

                //$.tableLineImgShow('http://img.bblocks.cn/c8958ea283cd4db38a773b863b9d9556.jpg');
            }else{
                layer.msg('开通群失败，请稍后再试', {icon: 5}, {anim:6});
            }
        }
    })

}

