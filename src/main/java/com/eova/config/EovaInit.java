/**
 * Copyright (c) 2013-2016, Jieven. All rights reserved.
 * <p/>
 * Licensed under the GPL license: http://www.gnu.org/licenses/gpl.txt
 * To use it on other terms please contact us at 1623736450@qq.com
 */
package com.eova.config;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import cn.hutool.db.Db;
import cn.hutool.db.Entity;
import cn.hutool.db.ds.simple.SimpleDataSource;
import com.eova.common.utils.xx;
import com.eova.common.utils.db.DbUtil;
import com.eova.common.utils.io.FileUtil;
import com.eova.common.utils.io.NetUtil;
import com.eova.common.utils.io.ZipUtil;
import com.eova.service.InstallService;
import com.google.common.collect.Lists;
import com.jfinal.kit.PathKit;

import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;

/**
 * 初始化
 * 参数来源：db，dev，default,jdbc.config
 * 原则：dev/default,jdbc.config ,db覆盖（先写入配置中缺失的，然后根据强制与否（目前是强制）除了 EovaInit.dbParamKeys 全部覆盖一次）
 */
@Slf4j
public class EovaInit {

	/**
	 * db参数keys(db参数不覆盖)
	 */
	private static final List<String> dbParamKeys = Lists.newArrayList("eova_url","eova_user","eova_pwd","main_url","main_user","main_pwd");

	/**
	 * 异步初始化JS插件包<br>
	 * 1.通过网络自动下载plugins.zip <br>
	 * 2.解压到webapp/plugins/ <br>
	 * 3.删除下载临时文件 <br>
	 */
	public static void initPlugins() {
		// 异步下载插件包
		Thread t = new Thread() {
			@Override
			public void run() {
				try {
					// 下载到Web根目录
					String zipPath = EovaConst.DIR_WEB + "plugins.zip";

					if (!FileUtil.isExists(EovaConst.DIR_PLUGINS)) {
						System.err.println("正在下载：" + EovaConst.PLUGINS_URL);
						NetUtil.download(EovaConst.PLUGINS_URL, zipPath);

						System.err.println("开始解压：" + zipPath);
						ZipUtil.unzip(zipPath, EovaConst.DIR_PLUGINS, null);
						System.err.println("已解压到：" + EovaConst.DIR_PLUGINS);

						FileUtil.delete(zipPath);
						System.err.println("清理下载Zip");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		t.start();
	}

	/**
	 * 初始化输出Oracle脚本
	 */
	public static void initCreateSql() {
		// 异步下载插件包
		Thread t = new Thread() {
			@Override
			public void run() {
				System.out.println("正在生成 eova oracle sql ing...");
				DbUtil.createOracleSql(xx.DS_EOVA, "EOVA%");
				// System.out.println("生成成功:/sql/oracle/eova.sql");

				System.out.println();
				System.out.println();

				System.out.println("正在生成 web oracle sql ing...");
				DbUtil.createOracleSql(xx.DS_MAIN, "%");
				// System.out.println("生成成功:/sql/oracle/*.sql");
			}
		};
		t.start();
	}

	/**
	 *  初始化静态配置
	 */
	public static void initStatic(){
		FileUtil.IMG_TYPE  = xx.getConfig("upload_img_type",FileUtil.IMG_TYPE);
		FileUtil.ALL_TYPE  = xx.getConfig("upload_file_type",FileUtil.ALL_TYPE);
	}
	
	/**
	 * 初始化配置
	 * 1、优先：default
	 * 2、不成功或者dev/test环境 则：dev
	 * 3、再次加载 jdbc.config
	 * 4、db加载，不覆盖db配置，如果配置了
	 */
	public static String initConfig(Map<String, String> props) {
		String resPath = PathKit.getRootClassPath() + File.separator;
		
		String path = "";
		
		// 加载默认配置
		boolean flag = loadConfig(props, resPath + "default");
		if (flag) {
			path = "/default";
			System.out.println("默认配置加载成功:(resources/default)\n");
		}

		//#环境标识：开发环境=DEV,测试环境=TEST,预发布环境=PRE,正式环境=PRD
		if("DEV".equalsIgnoreCase(props.get("env")) || "TEST".equalsIgnoreCase(props.get("env") ) || !flag){
			// 加载本地配置
			flag = loadConfig(props, resPath + "dev");
			if (flag) {
				path = "/dev";
				log.info("开发配置覆盖成功:(resources/dev)\n");
			}
		}
		
		//再次覆盖 根路径下： jdbc.config
		flag = loadConfig(props, resPath);
		if (flag) {
			log.info("加载了 根路径db配置");
		}

		/**
		 * 只有几个值会 进行db提取参数（原则上只有 jdbc.config 中的配置-6个 或者直接3个（ 主库））
		 */
		int paramsSzie = props.size();
		//if (paramsSzie>=3 && props.size()<=10){
			log.info("尝试再db加载资源覆盖，开始加载db配置！");
			flag = loadConfigByDb(props,true);
			if (flag) {
				log.info("加载了 db中资源配置");
			}
		//}
		initStatic();
		
		return path;
	}
	

	/**
	 * db加载配置（不覆盖db配置原则）
	 * @param props
	 * @param force 是否强制,true:则全部覆盖，否则:不存在就写入 (EovaInit.dbParamKeys 原则存在则不覆盖)
	 * @return
	 */
	private static boolean loadConfigByDb(Map<String, String> props,boolean force) {
		String bbUrl = xx.getConfig("eova_url");
		String bbUser = xx.getConfig("eova_user");
		String bbPwd = xx.getConfig("eova_pwd");
		int add = 0;
		if(!xx.isEmpty(bbUrl) && !xx.isEmpty(bbUser) && !xx.isEmpty(bbPwd)){
			try {
				DataSource ds = new SimpleDataSource(bbUrl, bbUser, bbPwd);
				Db db = cn.hutool.db.DbUtil.use(ds);
				List<Entity> result = db.query("SELECT * FROM bb_sys_params p WHERE 1=1 ORDER BY p.`group`");
				if (!xx.isEmpty(result)) {
					for (Entity e:result){
						String code = e.getStr("code");
						String value = e.getStr("value");
						if (!xx.isEmpty(code)) {
							props.putIfAbsent(code, value);

							if (force && !EovaInit.dbParamKeys.contains(code))//如果强制则排除掉排除的全部强制压入
								props.put(code, value);

							add ++;
						}
					}
				}
			}catch (Exception e){
				log.error("db加载配置异常：",e);
			}
		}else{
			log.warn("配置db未配置，db初始数据失败！");
		}

		if(add > 0) {
			//log.info("根：{}",PathKit.getWebRootPath());
			EovaConst.RESOURCE_MODE = "2";
			return true;
		}else
			return false;
	}

	/**
	 * 加载配置
	 *
	 * @param path
	 * @return
	 */
	private static boolean loadConfig(Map<String, String> props, String path) {
		if (!FileUtil.isDir(path)) {
			return false;
		}

		/*String bbUrl = "jdbc:mysql://101.132.243.52:33060/bb_h?characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&serverTimezone=GMT%2B8&nullCatalogMeansCurrent=true";
		String bbUser = "root";
		String bbPwd = "000000zJ";
		DataSource ds = new SimpleDataSource(bbUrl, bbUser, bbPwd);
		Db db = cn.hutool.db.DbUtil.use(ds);*/

		int fileNum = 0;
		File[] files = FileUtil.getFiles(path);
		//System.out.println("=================>");
		for (File file : files) {
			if (!file.getName().endsWith(".config")) {
				continue;
			}
			Properties properties = FileUtil.getProp(file);
			Set<Object> keySet = properties.keySet();
			for (Object ks : keySet) {
				String key = ks.toString();
				props.put(key, properties.getProperty(key));
				//System.out.println(key+":"+properties.getProperty(key)+":"+file.getName());

				/**
				 try {
				 Entity record = Entity.create()
				 .setTableName("bb_sys_params")
				 .set("code", key)
				 .set("name", key)
				 .set("value", properties.getProperty(key))
				 .set("remark", key)
				 .set("group", file.getName().replace(".config", ""));
				 db.insert(record);
				 }catch (Exception e){
				 e.printStackTrace();
				 }
				 **/
			}
			System.out.println(file.getName());
			fileNum ++;
		}
		//System.out.println("<=================");
		if(fileNum>0)
			return true;
		else
			return false;
	}
}