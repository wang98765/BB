package com.eova.config.satoken;


import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONReader;
import com.alibaba.fastjson2.JSONWriter;
import com.alibaba.fastjson2.filter.Filter;
import com.jfinal.kit.LogKit;
import com.jfinal.plugin.redis.serializer.FstSerializer;
import com.jfinal.plugin.redis.serializer.ISerializer;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.nustaq.serialization.FSTObjectInput;
import org.nustaq.serialization.FSTObjectOutput;
import redis.clients.jedis.util.SafeEncoder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * <p>Project: bb-project - JsonSerializer</p>
 * <p>描述：json序列化工具（基于fastjson）</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/10 17:41
 * @Version 1.0
 * @since 8
 */
@Slf4j
@Builder
public class JsonSerializer implements ISerializer {
    public JSONWriter.Feature[] features = new JSONWriter.Feature[]{
            JSONWriter.Feature.WriteClassName,
            JSONWriter.Feature.FieldBased,
            JSONWriter.Feature.ReferenceDetection,
            JSONWriter.Feature.NotWriteDefaultValue,
            JSONWriter.Feature.WriteNameAsSymbol,
            JSONWriter.Feature.WriteEnumsUsingName
    };

    private static final Filter autoTypeFilter;


    static {
        autoTypeFilter = JSONReader.autoTypeFilter(
                // 按需加上需要支持自动类型的类名前缀，范围越小越安全， 我这个就比较过分了，直接全部放开，哈哈
                "com.",
                "org.",
                "java."
        );
    }





    Charset defaultCharset =  Charset.forName("utf-8");


    public byte[] keyToBytes(String key) {
        return SafeEncoder.encode(key);
    }

    public String keyFromBytes(byte[] bytes) {
        return SafeEncoder.encode(bytes);
    }

    public byte[] fieldToBytes(Object field) {
        return valueToBytes(field);
    }

    public Object fieldFromBytes(byte[] bytes) {
        return valueFromBytes(bytes);
    }

    public byte[] valueToBytes(Object value) {
        if(value == null)
            return new byte[0];

        log.info("value:{}",value);
        /*JSONWriter.
        //JSON.toJSONString(value, JSONWriter.Feature.);
        JSONWriter*/
        String json = JSON.toJSONString(value,JSONWriter.Feature.WriteClassName);
        log.info("json:{}",json);
        //byte[] byteData = JSON.toJSONBytes(value,JSONWriter.Feature.WriteClassName);

        return JSON.toJSONBytes(value, features);

        //return  json.getBytes(defaultCharset);
    }

    public Object valueFromBytes(byte[] bytes) {
        if(bytes == null || bytes.length == 0)
            return null;
        log.info("bytes:{}",new String(bytes));

        return JSON.parse(bytes,JSONReader.Feature.UseDefaultConstructorAsPossible,
                JSONReader.Feature.UseNativeObject,
                JSONReader.Feature.FieldBased
                );


       // return JSON.parse(new String(bytes,defaultCharset), JSONReader.Feature.SupportAutoType);
    }

  /*  public static RedisTemplate<String, Object> redisTemplate(LettuceConnectionFactory lettuceConnectionFactory) {
        lettuceConnectionFactory.setShareNativeConnection(false);
        RedisTemplate<String, Object> rt = new RedisTemplate<>();
        // 设置连接工厂
        rt.setConnectionFactory(lettuceConnectionFactory);
        // 设置 key 的序列化
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        rt.setKeySerializer(stringRedisSerializer);
        rt.setHashKeySerializer(stringRedisSerializer);
        // 创建 JSON 序列化工具
        Jackson2JsonRedisSerializer<Object> jsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        *//*ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        mapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance,
                ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
        jsonRedisSerializer.setObjectMapper(mapper);*//*
        // 设置 value 的序列化
        rt.setValueSerializer(jsonRedisSerializer);
        rt.setHashValueSerializer(jsonRedisSerializer);
        rt.afterPropertiesSet();
        return rt;
    }*/


}
