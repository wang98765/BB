package com.eova.config.satoken;

import cn.dev33.satoken.dao.SaTokenDao;
import cn.dev33.satoken.jfinal.SaJdkSerializer;
import cn.dev33.satoken.util.SaFoxUtil;
import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;
import com.jfinal.plugin.redis.serializer.FstSerializer;
import com.jfinal.plugin.redis.serializer.ISerializer;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * <p>Project: bb-project - SaTokenDaoRedisWithDefaultFst</p>
 * <p>描述：默认换了序列化方式（fst）</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/7 15:45
 * @Version 1.0
 * @since 8
 */
public class SaTokenDaoRedisWithDefaultFst implements SaTokenDao {
    protected Cache redis;
    protected ISerializer serializer;

    public SaTokenDaoRedisWithDefaultFst(String confName) {
        this(confName,new FstSerializer());
    }

    public SaTokenDaoRedisWithDefaultFst(String confName, ISerializer serializer) {
        this.redis = Redis.use(confName);
        this.serializer = serializer;
    }

    public String get(String key) {
        Jedis jedis = this.getJedis();

        String var3;
        try {
            var3 = jedis.get(key);
        } finally {
            this.close(jedis);
        }

        return var3;
    }

    public void set(String key, String value, long timeout) {
        if (timeout != 0L && timeout > -2L) {
            Jedis jedis = this.getJedis();

            try {
                if (timeout == -1L) {
                    jedis.set(key, value);
                } else {
                    jedis.setex(key, timeout, value);
                }
            } finally {
                this.close(jedis);
            }

        }
    }

    public void update(String key, String value) {
        long expire = this.getTimeout(key);
        if (expire != -2L) {
            this.set(key, value, expire);
        }
    }

    public void delete(String key) {
        Jedis jedis = this.getJedis();

        try {
            jedis.del(key);
        } finally {
            this.close(jedis);
        }

    }

    public long getTimeout(String key) {
        Jedis jedis = this.getJedis();

        long var3;
        try {
            var3 = jedis.ttl(key);
        } finally {
            this.close(jedis);
        }

        return var3;
    }

    public void updateTimeout(String key, long timeout) {
        if (timeout == -1L) {
            long expire = this.getTimeout(key);
            if (expire != -1L) {
                this.set(key, this.get(key), timeout);
            }

        } else {
            Jedis jedis = this.getJedis();

            try {
                jedis.expire(key, timeout);
            } finally {
                this.close(jedis);
            }

        }
    }

    public Object getObject(String key) {
        Jedis jedis = this.getJedis();

        Object var3;
        try {
            var3 = this.valueFromBytes(jedis.get(this.keyToBytes(key)));
        } finally {
            this.close(jedis);
        }

        return var3;
    }

    public void setObject(String key, Object object, long timeout) {
        if (timeout != 0L && timeout > -2L) {
            Jedis jedis = this.getJedis();

            try {
                if (timeout == -1L) {
                    jedis.set(this.keyToBytes(key), this.valueToBytes(object));
                } else {
                    jedis.setex(this.keyToBytes(key), timeout, this.valueToBytes(object));
                }
            } finally {
                this.close(jedis);
            }

        }
    }

    public void updateObject(String key, Object object) {
        long expire = this.getObjectTimeout(key);
        if (expire != -2L) {
            this.setObject(key, object, expire);
        }
    }

    public void deleteObject(String key) {
        Jedis jedis = this.getJedis();

        try {
            jedis.del(this.keyToBytes(key));
        } finally {
            this.close(jedis);
        }

    }

    public long getObjectTimeout(String key) {
        Jedis jedis = this.getJedis();

        long var3;
        try {
            var3 = jedis.ttl(this.keyToBytes(key));
        } finally {
            this.close(jedis);
        }

        return var3;
    }

    public void updateObjectTimeout(String key, long timeout) {
        if (timeout == -1L) {
            long expire = this.getObjectTimeout(key);
            if (expire != -1L) {
                this.setObject(key, this.getObject(key), timeout);
            }

        } else {
            Jedis jedis = this.getJedis();

            try {
                jedis.expire(this.keyToBytes(key), timeout);
            } finally {
                this.close(jedis);
            }

        }
    }

    public List<String> searchData(String prefix, String keyword, int start, int size) {
        Set<String> keys = this.redis.keys(prefix + "*" + keyword + "*");
        List<String> list = new ArrayList(keys);
        return SaFoxUtil.searchList(list, start, size);
    }

    public Jedis getJedis() {
        return this.redis.getJedis();
    }

    public void close(Jedis jedis) {
        if (jedis != null) {
            jedis.close();
        }

    }

    protected byte[] keyToBytes(Object key) {
        return key.toString().getBytes();
    }

    protected byte[] valueToBytes(Object value) {
        return this.serializer.valueToBytes(value);
    }

    protected Object valueFromBytes(byte[] bytes) {
        return this.serializer.valueFromBytes(bytes);
    }
}
