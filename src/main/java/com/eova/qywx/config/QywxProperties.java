package com.eova.qywx.config;

import cn.hutool.core.collection.CollUtil;
import com.dtflys.forest.Forest;
import com.dtflys.forest.config.ForestConfiguration;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * 属性
 *
 */
@Slf4j
@Data
public class QywxProperties {

	private static final QywxProperties INSTANCE = new QywxProperties();

	public static final QywxProperties getInstance(){
		return INSTANCE;
	}

	private QywxProperties(){
	}

	/**
	 * 启动状态 1=成功 ，0=未启动,2=启动中
	 */
	@Getter(value=AccessLevel.PRIVATE)
	@Setter(value=AccessLevel.PRIVATE)
	private AtomicInteger INIT_SUC = new AtomicInteger(0) ;

	/**
	 * token存活时间（h），一般是2小时(必须)
	 */
	private Integer tokenTime = 2;

	/**
	 * 企业id(必须)
	 */
	private String corpid;

	/**
	 *秘钥（可能存在多个），此处是应用的(必须)
	 */
	private String corpsecret;

	/**
	 * 企业微信 ，前缀地址（结尾/不要）(必须)
	 */
	private String wxBase;

    /**
	 * worktool 前缀地址（结尾/不要）(必须)
	 */
	private String wtBase;


	/**
	 * 机器人以及客服账户(必须)
	 * ai:
	 *   qywx:
	 *     robotWithWxAccs:
	 *       - robotId: xxxx
	 *         wxNames:
	 *          - 小a
	 *          - 小b
	 *       - robotId:
	 *         wxNames:
	 *           - 小c
	 *
	 */
	private List<RobotWithWxAcc> robotWithWxAccs;



	/**
	 * 有效天数（默认7天）
	 */
	private Integer qrcodeValidDays = 7;


	public int getInitState(){
		return INIT_SUC.get();
	}

	/**
     * 初始化参数
	 */
	public void init(){
		int cState = getInitState();
		if (cState != 0){
			log.info("状态错误，不启动！");
			return ;
		}
		INIT_SUC.set(2);
		if(this.corpid != null
		&&this.corpsecret != null
		&& wxBase != null && wtBase != null
		&& CollUtil.isNotEmpty(robotWithWxAccs)){
			ForestConfiguration configuration = Forest.config();
			// 连接池最大连接数
			configuration.setMaxConnections(500);
			// 连接超时时间，单位为毫秒
			configuration.setConnectTimeout(3000);
			// 数据读取超时时间，单位为毫秒
			configuration.setReadTimeout(5000);

			// 设置全局变量: wxBase -> wxBase
			configuration.setVariableValue("wxBase", wxBase);
			// 设置全局变量: wtBase -> wtBase
			configuration.setVariableValue("wtBase", wtBase);

			/*// 获取 Forest 全局配置对象
			configuration = ForestConfiguration.configuration();
			// 设置全局变量: wxBase -> wxBase
			configuration.setVariableValue("wxBase", wxBase);
			// 设置全局变量: wtBase -> wtBase
			configuration.setVariableValue("wtBase", wtBase);*/

			/*configuration = Forest.config();
			// 连接池最大连接数
			configuration.setMaxConnections(500);
			// 连接超时时间，单位为毫秒
			configuration.setConnectTimeout(2000);
			// 数据读取超时时间，单位为毫秒
			configuration.setReadTimeout(5000);*/


			INIT_SUC.set(1);
			log.warn("企业微信启动完成！");
		}else{
			log.warn("参数确实，启动失败！");
			INIT_SUC.set(0);
		}
	}



	@NoArgsConstructor
	@AllArgsConstructor
	@Data
	public static class RobotWithWxAcc{

		/**
		 * 对应的机器人
		 */
		private String robotId;

		/**
		 * 微信名
		 */
		private List<String> wxNames;
	}
}
