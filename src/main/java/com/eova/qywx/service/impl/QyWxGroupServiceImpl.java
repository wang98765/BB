package com.eova.qywx.service.impl;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.eova.qywx.callback.CreateCustomGroupCallBack;
import com.eova.qywx.config.QywxProperties;
import com.eova.qywx.service.AppRobotService;
import com.eova.qywx.service.CustomGroupService;
import com.eova.qywx.service.QyWxGroupService;
import com.google.common.base.Splitter;
import com.eova.qywx.comm.vo.*;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - QyWxGroupService</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 17:10
 * @Version 1.0
 * @since 8
 */
@RequiredArgsConstructor(staticName = "newInstance", access = AccessLevel.PUBLIC, onConstructor_ = {@Deprecated})
@Slf4j
public class QyWxGroupServiceImpl implements QyWxGroupService {

    private final CustomGroupService customGroupService;
    private final QywxProperties qywxProperties;

    /**
     * 创建群的回调
     */
    private final CreateCustomGroupCallBack createCustomGroupCallBack;

    @Override
    public String getGroupName(String name, Long groupOrder) {
        String finalGroupName = name+"_"+groupOrder;
        return finalGroupName;
    }

    public Pair<Long,String> groupIdByAnalysisGroupName(@NonNull String groupName){
        List<String> stringList = Splitter.on("_").splitToList(groupName);
        if(stringList.size() == 2 && NumberUtil.isLong(stringList.get(1)) ){
            return Pair.of(Long.parseLong(stringList.get(1)),stringList.get(0));
        }
        return null;
    }



    /**
     * 接收到创建完成群了
     * @param param type== 206为建群完成的
     */
    @Override
    public void onGroupCreateReceived(WTCallBackReceiveVo param){
        if (param.getType().equals(206)){//创建群成功
            String theGroupName = param.getGroupName();

            Pair<Long,String>  groupPair = groupIdByAnalysisGroupName(theGroupName);
            if(groupPair != null) {
                log.debug("合规的微信名，将匹配本地数据");
                    Long groupDbId = groupPair.getKey();
                    log.info("提取到群DB id:{}",groupDbId);
                    //执行db，查询数据是否存在此群（创建中）
                    boolean exists = createCustomGroupCallBack.isGroupExists(groupDbId);
                    if(exists){
                      String groupId =  groupIdByName(theGroupName);
                      log.info("企业微信查询到群：{}，id为：{}",theGroupName,groupId);
                      if(StrUtil.isNotEmpty(groupId)){
                        //写入db结束，后续提取二维码，后面单独接口提取
                          createCustomGroupCallBack.updateQywxGroupId(groupDbId, groupId);
                      }
                    }else {
                        log.warn("群：{}（{}） 本地未匹配到，放弃处理！",groupDbId,theGroupName);
                    }
            }
        }
    }



    @Override
    public String groupIdByName(String theGroupName){
            String groupId = null;
            String cursor = null;
            while(true){
                CustomGroupListResultVo resultVo = customGroupService.groupList(null,cursor);

                //本组群详情
                for (CustomGroupListResultVo.GroupChat groupChat:resultVo.getGroup_chat_list()){
                    String chatGroupId = groupChat.getChat_id();
                    CustomGroupInfoResultVo.GroupChat groupChatDetail = customGroupService.groupInfo(chatGroupId);

                    String groupName = groupChatDetail.getName();
                    if(theGroupName.equals(groupName)){
                        groupId = groupChatDetail.getChat_id();
                        break;
                    }
                }

                if(resultVo.getGroup_chat_list().size() < 100 || groupId !=null ){
                    break;
                }
                cursor = resultVo.getNext_cursor();
            }
            return groupId;
    }

}
