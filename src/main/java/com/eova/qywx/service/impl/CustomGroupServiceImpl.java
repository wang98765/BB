package com.eova.qywx.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.eova.model.User;
import com.eova.qywx.config.QywxProperties;
import com.eova.qywx.service.CustomGroupService;
import com.eova.qywx.service.WxTokenService;
import com.google.common.collect.Lists;
import com.eova.qywx.comm.WxGroupClient;
import com.eova.qywx.comm.vo.*;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - GroupService</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 11:36
 * @Version 1.0
 * @since 8
 */
@RequiredArgsConstructor(staticName = "newInstance", access = AccessLevel.PUBLIC, onConstructor_ = {@Deprecated})
@Slf4j
public class CustomGroupServiceImpl implements CustomGroupService {

    private final WxGroupClient wxGroupClient;
    private final WxTokenService tokenService;
    private final QywxProperties qywxProperties;


    /**
     * 创建机器人的群(含固定另外一个人-客服经理？)
     * @param groupName
     * @param userIds
     * @return
     */
    @Override
    public String create(@NonNull String groupName, @NonNull List<String> userIds){
        String token = tokenService.getTokenWithErr();
        GroupCreateResultVo resultVo = wxGroupClient.create(GroupCreateVo
                .builder()
                        .chatid(null)
                        .name(groupName)
                        .userlist(userIds)
                        .owner(userIds.get(0))
                .build(),token);

        return resultVo.getChatid();
    }


    /**
     * 客户群列表
     * @param ownerId 群主
     * @param cursor
     */
    @Override
    public CustomGroupListResultVo groupList(String ownerId, String cursor){
        String token = tokenService.getTokenWithErr();

        CustomGroupListVo.OwnerFilter owner_filter = null;
        if(StrUtil.isNotEmpty(ownerId)){
            owner_filter = CustomGroupListVo.OwnerFilter
                    .builder()
                    .userid_list(Lists.newArrayList(ownerId))
                    .build();
        }

        CustomGroupListVo req = CustomGroupListVo
                .builder()
                .cursor(cursor)
                .owner_filter(owner_filter)
                .build();

        CustomGroupListResultVo resultVo = wxGroupClient.list(req,token);
        if(!resultVo.getErrcode().equals(0)) {
            log.error("groupList err:{}", JSONUtil.toJsonStr(resultVo));
        }
        return resultVo;
    }

    /**
     * 客户群 信息
     * @param chatGroupId
     * @return
     */
   // @Cacheable( key = "'groupInfo&chatGroupId='+#p0")
    @Override
    public CustomGroupInfoResultVo.GroupChat groupInfo(String chatGroupId){
        String token = tokenService.getTokenWithErr();

        CustomGroupInfoVo req = CustomGroupInfoVo
                .builder()
                .chat_id(chatGroupId)
                .build();

        CustomGroupInfoResultVo resultVo = wxGroupClient.groupInfo(req,token);
        if(!resultVo.getErrcode().equals(0)) {
            log.error("groupList err:{}", JSONUtil.toJsonStr(resultVo));
        }
        return resultVo.getGroup_chat();
    }


    /**
     * 配置客户群进群方式
     * @param chatGroupId
     */
    @Override
    public String toJoin(String chatGroupId){
        String token = tokenService.getTokenWithErr();

        CustomGroupToJoinVo req = CustomGroupToJoinVo
                .builder()
                .chat_id_list(Lists.newArrayList(chatGroupId))
                .build();
        CustomGroupToJoinResultVo resultVo = wxGroupClient.toJoin(req,token);
        if(!resultVo.getErrcode().equals(0)) {
            log.error("toJoin err:{}", JSONUtil.toJsonStr(resultVo));
        }
        return resultVo.getConfig_id();
    }

    @Override
    public CustomGroupJoinInfoResultVo.JoinWay joinInfo(String configId){
        String token = tokenService.getTokenWithErr();

        CustomGroupJoinInfoVo req = CustomGroupJoinInfoVo
                .builder()
                .config_id(configId)
                .build();
        CustomGroupJoinInfoResultVo resultVo = wxGroupClient.joinInfo(req,token);
        if(!resultVo.getErrcode().equals(0)) {
            log.error("joinInfo err:{}", JSONUtil.toJsonStr(resultVo));
        }
        return resultVo.getJoin_way();
    }

    public Integer getQrcodeValidDays(){
       return qywxProperties.getQrcodeValidDays();
    }



}

