package com.eova.qywx.service.impl;

import com.eova.config.EovaConfig;
import com.eova.qywx.comm.TokenClient;
import com.eova.qywx.comm.vo.TokenResultVo;
import com.eova.qywx.config.QywxProperties;
import com.eova.qywx.service.CustomGroupService;
import com.eova.qywx.service.WxTokenService;
import com.jfinal.plugin.redis.Redis;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


import javax.annotation.Resource;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - WxTokenServiceImpl</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 11:55
 * @Version 1.0
 * @since 8
 */
@RequiredArgsConstructor(staticName = "newInstance", access = AccessLevel.PUBLIC, onConstructor_ = {@Deprecated})
@Slf4j
public class WxTokenServiceImpl implements WxTokenService {
    private  static final String MODULE = "WxToken";

    private final TokenClient tokenClient;
    private final QywxProperties qywxProperties;

    /**
     * 获取token（可以缓存 120分钟）,最好锁一下
     * @return
     */
    @Override
    public String getToken() {
        String key = EovaConfig.SYS_MODULE+"::"+MODULE+"::wxToken";

        String token = (String)Redis.use().get(key);
        if(token == null){
            TokenResultVo resultVo = tokenClient.token(qywxProperties.getCorpid(),qywxProperties.getCorpsecret());
            log.info("getToken:{}",resultVo);
            token = resultVo.getAccess_token();
            if(token != null){
                Redis.use().setex(key,qywxProperties.getTokenTime()*60*60,token);
            }
        }

        return token;
    }

    /**
     * 业务提取token（）
     * @return
     */
    @Override
    public String getTokenWithErr() {
        String theToken = this.getToken();
        if(theToken == null){
            log.error("提取企业微信Access_token，失败！");
            throw new RuntimeException("系统故障，请稍后再试！");
        }
        return  theToken;
    }
}
