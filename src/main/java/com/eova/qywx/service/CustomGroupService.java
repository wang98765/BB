package com.eova.qywx.service;

import com.dtflys.forest.Forest;
import com.eova.qywx.comm.WorkToolClient;
import com.eova.qywx.comm.WxGroupClient;
import com.eova.qywx.comm.vo.CustomGroupInfoResultVo;
import com.eova.qywx.comm.vo.CustomGroupJoinInfoResultVo;
import com.eova.qywx.comm.vo.CustomGroupListResultVo;
import com.eova.qywx.config.QywxProperties;
import com.eova.qywx.service.impl.AppRobotServiceImpl;
import com.eova.qywx.service.impl.CustomGroupServiceImpl;
import lombok.NonNull;

import java.util.List;

/**
 * <p>Project: jd-aigc - CustomGroupService</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/14 15:45
 * @Version 1.0
 * @since 8
 */
public interface CustomGroupService {

    public static CustomGroupService getCustomGroupService(){
        WxGroupClient wxGroupClient = Forest.client(WxGroupClient.class);
        WxTokenService wxTokenService = WxTokenService.getWxTokenService();
        QywxProperties qywxProperties = QywxProperties.getInstance();

        return CustomGroupServiceImpl.newInstance(wxGroupClient,wxTokenService,qywxProperties);
    }

    String create(@NonNull String groupName, @NonNull List<String> userIds);

    CustomGroupListResultVo groupList(String ownerId, String cursor);

    // @Cacheable( key = "'groupInfo&chatGroupId='+#p0")
    CustomGroupInfoResultVo.GroupChat groupInfo(String chatGroupId);

    String toJoin(String chatGroupId);

    CustomGroupJoinInfoResultVo.JoinWay joinInfo(String configId);


    /**
     * qrcode默认的有效天数（默认7）
     * @return
     */
    Integer getQrcodeValidDays();
}
