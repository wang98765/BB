package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - UserGetVo</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 20:54
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class UserGetVo {

    /**
     * 用于分页查询的游标，字符串类型，由上一次调用返回，首次调用不填
     */
    //private String cursor;

    /**
     *分页，预期请求的数据量，取值范围 1 ~ 10000
     */
    private Integer limit;
}
