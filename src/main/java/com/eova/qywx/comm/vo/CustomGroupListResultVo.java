package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - CustomGroupListResultVo</p>
 * <p>描述：获取客户群列表 查询结果</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 18:43
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class CustomGroupListResultVo extends BaseResponse{
    /**
     * 客户群列表
     */
    private List<GroupChat> group_chat_list;

    /**
     * 分页游标，下次请求时填写以获取之后分页的记录。如果该字段返回空则表示已没有更多数据
     */
    private String next_cursor;


    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @SuperBuilder
    public static class GroupChat{

        /**
         * 客户群ID
         */
        private String chat_id;

        /**
         * 客户群跟进状态。
         * 0 - 跟进人正常
         * 1 - 跟进人离职
         * 2 - 离职继承中
         * 3 - 离职继承完成
         */
        private Integer status;
    }
}
