package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - CustomGroupListVo</p>
 * <p>描述：获取客户群列表 查询参数</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 18:38
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class CustomGroupListVo {

    /**
     *客户群跟进状态过滤。
     * 0 - 所有列表(即不过滤)
     * 1 - 离职待继承
     * 2 - 离职继承中
     * 3 - 离职继承完成
     *
     * 默认为0
     */
    @Builder.Default
    private Integer status_filter = 0;

    /**
     * 	群主过滤。
     * 如果不填，表示获取应用可见范围内全部群主的数据（但是不建议这么用，如果可见范围人数超过1000人，为了防止数据包过大，会报错 81017）
     */
    private OwnerFilter owner_filter;

    /**
     * 用于分页查询的游标，字符串类型，由上一次调用返回，首次调用不填
     */
    private String cursor;

    /**
     * 分页，预期请求的数据量，取值范围 1 ~ 1000
     */
    @Builder.Default
    private Integer limit = 100;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @SuperBuilder
    public static class OwnerFilter{

        /**
         * 用户ID列表。最多100个
         */
        private List<String> userid_list;
    }
}
