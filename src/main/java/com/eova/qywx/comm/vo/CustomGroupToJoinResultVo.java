package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - CustomGroupToJoinVo</p>
 * <p>描述：客户群「加入群聊」to加入响应</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 18:51
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class CustomGroupToJoinResultVo extends BaseResponse{

    /**
     * 配置id
     */
    private String config_id;

}
