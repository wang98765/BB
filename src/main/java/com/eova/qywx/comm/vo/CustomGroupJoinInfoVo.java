package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - CustomGroupJoinInfoVo</p>
 * <p>描述：进群方式配置 查询参数</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 18:58
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class CustomGroupJoinInfoVo {

    /**
     * 联系方式的配置id
     */
    private String config_id;
}
