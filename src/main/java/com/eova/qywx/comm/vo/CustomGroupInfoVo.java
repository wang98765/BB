package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - CustomGroupInfoVo</p>
 * <p>描述：获取客户群详情 查询参数</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 18:38
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class CustomGroupInfoVo {

    /**
     * 客户群ID
     */
    private String chat_id;

    /**
     * 是否需要返回群成员的名字group_chat.member_list.name。0-不返回；1-返回。默认不返回
     */
    @Builder.Default
    private Integer need_name = 1;

}
