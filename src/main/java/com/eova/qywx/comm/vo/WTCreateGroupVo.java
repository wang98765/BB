package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - WTCreateGroupVo</p>
 * <p>描述：创建外部群参数</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 17:17
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class WTCreateGroupVo {
    /**
     * 未知，固定2
     */
    @Builder.Default
    private Integer socketType = 2;

    /**
     * 群参数（支持多个）
     */
    private List<WTCreateOneGroupVo> list;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @SuperBuilder
    public static class WTCreateOneGroupVo{

        /**
         * 206-固定值
         */
        @Builder.Default
        private Integer type = 206;

        /**
         * 群名
         */
        private String groupName;

        /**
         * 要拉入群的成员昵称
         */
        private List<String> selectList;

        /**
         * 群的群公告(选填)
         */
        private String groupAnnouncement;

        /**
         * 修改群备注(选填) api提取不到暂时不用了吧
         */
        private String groupRemark;
    }
}
