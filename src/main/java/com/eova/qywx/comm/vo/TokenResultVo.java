package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - TokenResultVo</p>
 * <p>描述：获取token响应</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 11:53
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class TokenResultVo extends BaseResponse{

    /**
     *获取到的凭证，最长为512字节
     */
    private String access_token;
    /**
     *凭证的有效时间（秒
     */
    private Integer expires_in;
}
