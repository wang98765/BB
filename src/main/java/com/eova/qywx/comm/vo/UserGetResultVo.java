package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - UserGetResultVo</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 20:56
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class UserGetResultVo extends BaseResponse{

    /**
     * 分页游标，下次请求时填写以获取之后分页的记录。如果该字段返回空则表示已没有更多数据
     */
    private String next_cursor;

    /**
     * 用户-部门关系列表
     */
    public List<DeptUser> dept_user;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @Builder
    public static class DeptUser{

        private String userid;

        private String department;
    }
}
