package com.eova.qywx.comm;

import com.dtflys.forest.annotation.Body;
import com.dtflys.forest.annotation.BodyType;
import com.dtflys.forest.annotation.Post;
import com.dtflys.forest.annotation.Var;
import com.eova.qywx.comm.vo.*;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - UserClient</p>
 * <p>描述：用户接口</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 20:52
 * @Version 1.0
 * @since 8
 */
public interface UserClient {

    @Post(url = "${wxBase}/cgi-bin/user/list_id?access_token={accessToken}", contentType = "application/json")
    @BodyType(type = "json")
    UserGetResultVo list(@Body UserGetVo req, @Var("accessToken") String accessToken );


/*    @Post(url = "#{wecom.qywxBase}/cgi-bin/user/list_id?access_token={accessToken}", contentType = "application/json")
    @BodyType(type = "json")
    UserGetResultVo list(@Body UserGetVo req, @Var("accessToken") String accessToken );*/

}
