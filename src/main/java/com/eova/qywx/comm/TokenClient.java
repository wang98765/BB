package com.eova.qywx.comm;

import com.dtflys.forest.annotation.Get;
import com.dtflys.forest.annotation.Var;
import com.eova.qywx.comm.vo.*;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - WxClient</p>
 * <p>描述：企业群相关接口
 * https://developer.work.weixin.qq.com/document/path/91039
 * </p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 10:40
 * @Version 1.0
 * @since 8
 */
public interface TokenClient {

    /**
     * 创建群
     * @param corpid  企业ID，获取方式参考
     * @param corpsecret 应用的凭证密钥，注意应用需要是启用状态
     * @return
     */
    @Get(url = "${wxBase}/cgi-bin/gettoken?corpid={corpid}&corpsecret={corpsecret}")
    TokenResultVo token(@Var("corpid") String corpid, @Var("corpsecret") String corpsecret );


}
