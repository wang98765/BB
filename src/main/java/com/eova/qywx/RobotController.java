package com.eova.qywx;

import com.eova.common.Response;
import com.eova.config.EovaConst;
import com.eova.exception.BusinessException;
import com.eova.model.AiRobot;
import com.eova.model.User;
import com.eova.qywx.service.AppRobotService;
import com.jfinal.core.Controller;

/**
 * <p>Project: bb-project - RobotController</p>
 * <p>描述：机器相关操作（主要开通微信）</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/15 22:59
 * @Version 1.0
 * @since 8
 */
public class RobotController extends Controller {

    /**
     * 申请开通企业微信群聊（目前只有一个群）
     */
    public void sbumitQywx() {
        User user = getAttr(EovaConst.USER);
        Long robotId = this.getParaToLong("robotId");

        renderJson( Response.sucData(AppRobotService.getAppRobotService().sbumitQywx(robotId,user))  );
    }

    public void getQywxQrcode() {
        User user = getAttr(EovaConst.USER);
        Long robotId = this.getParaToLong("robotId");
        AiRobot robot = AiRobot.dao.getValidRobot(robotId);
        if(robot == null)
            throw new BusinessException("无此机器人信息");
        AppRobotService.getAppRobotService().getQrCodeAndSave(robot);

        renderJson(Response.sucData(robot)  );
    }
}
