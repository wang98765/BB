package com.eova.qywx.callback.impl;

import com.eova.qywx.callback.CreateCustomGroupCallBack;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>Project: jd-aigc - CreateCustomGroupCallBackNone</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/14 14:38
 * @Version 1.0
 * @since 8
 */
@Slf4j
public class CreateCustomGroupCallBackNone implements CreateCustomGroupCallBack {


    /**
     * 判断群是否存在（存在的话（最好 创建企业微信群中 才认为存在），后续会调用更新接口推送真的qywxId）
     *
     * @param groupDbId
     * @return
     */
    @Override
    public boolean isGroupExists(Long groupDbId) {
        log.warn("用户未实现 CreateCustomGroupCallBack！");
        return false;
    }

    /**
     * 更新企业微信ID
     * 更新完成会后 算是一条完整的群数据
     *
     * @param groupDbId
     * @param qywxGroupId
     * @return
     */
    @Override
    public int updateQywxGroupId(Long groupDbId, String qywxGroupId) {
        log.warn("用户未实现 CreateCustomGroupCallBack！");
        return 0;
    }
}
