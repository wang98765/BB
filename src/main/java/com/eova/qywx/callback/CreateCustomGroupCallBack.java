package com.eova.qywx.callback;

import com.dtflys.forest.Forest;
import com.eova.qywx.callback.impl.CreateCustomGroupCallBackImpl;
import com.eova.qywx.comm.WorkToolClient;
import com.eova.qywx.service.AppRobotService;
import com.eova.qywx.service.QyWxGroupService;
import com.eova.qywx.service.impl.AppRobotServiceImpl;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - CreateCustomGroupBack</p>
 * <p>描述：创建群的回调</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/14 14:03
 * @Version 1.0
 * @since 8
 */
public interface CreateCustomGroupCallBack {

    public static CreateCustomGroupCallBack getCreateCustomGroupCallBack(){
        return CreateCustomGroupCallBackImpl.newInstance();
    }


    /**
     * 判断群是否存在（存在的话（最好 创建企业微信群中 才认为存在），后续会调用更新接口推送真的qywxId）
     * @param groupDbId
     * @return
     */
    boolean isGroupExists(Long groupDbId);

    /**
     * 更新企业微信ID
     * 更新完成会后 算是一条完整的群数据
     * @param groupDbId
     * @param qywxGroupId
     * @return
     */
    int updateQywxGroupId(Long groupDbId,String qywxGroupId);

}
