package com.eova.model;

import com.eova.common.base.BaseModel;

import java.util.List;


/**
 * <p>Project: bb-project - AiRobot</p>
 * <p>描述：ai机器人</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/6 9:24
 * @Version 1.0
 * @since 8
 */
public class AiRobot extends BaseModel<AiRobot> {
	public static final AiRobot dao = new AiRobot();


	public AiRobot getValidRobot(Long robotId){
		return this.queryFisrtByCache("SELECT  * FROM ai_robot r WHERE r.`is_delete`=0 AND r.`state`=1 AND r.id=?",robotId);
	}

	/**
	 * 模型关联的一个
	 * @param aiModelId
	 * @return
	 */
	public AiRobot getValidModelRobot(Long aiModelId){
		return this.findFirst("select * from ai_robot r where r.`state`=1 and r.`is_delete`=0 and find_in_set(?,r.`model_ids`) order by id desc limit 1 ",aiModelId);
	}
}
