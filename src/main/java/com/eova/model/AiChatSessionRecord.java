package com.eova.model;

import com.eova.common.base.BaseModel;

import java.util.List;


/**
 * <p>Project: bb-project - AiChatSessionRecord</p>
 * <p>描述：ai会话明细</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/6 9:24
 * @Version 1.0
 * @since 8
 */
public class AiChatSessionRecord extends BaseModel<AiChatSessionRecord> {
	public static final AiChatSessionRecord dao = new AiChatSessionRecord();


	/**
	 * 会话的全部消息记录（后面分页吧）
	 * @param sessionId
	 * @return
	 */
	public List<AiChatSessionRecord> queryAllRecords(Long sessionId,int limit) {
		return this.find("select * from" +
						" (" +
						" select * from ai_chat_session_record r where r.`session_id`= ? order by id desc limit ?" +
						" ) v order by v.id asc;",
				sessionId,limit);
	}
}
