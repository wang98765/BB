package com.eova.model;

import com.eova.common.base.BaseModel;
import com.eova.common.utils.db.DbUtil;
import com.eova.common.utils.xx;
import com.jfinal.plugin.activerecord.Db;

import java.util.List;

/**
 * 角色显示字段(如无则无配置，不控制) 未加缓存
 * @author jin
 *
 */
public class SysParams extends BaseModel<SysParams> {

    /**
     * ai组
     */
    public static final String AI_GROUP = "ai";
    /**
     * 应用组
     */
    public static final String APP_GROUP = "domain";

    public static final String PL_GROUP = "eova";

    private static final long serialVersionUID = -1894335434198017392L;

    public static final SysParams dao = new SysParams();

    /**
     * 根据code查询
     * @param code
     * @return
     */
    public SysParams queryByCode(String code) {
        return this.queryFisrtByCache("select * from bb_sys_params p where p.`code`=?",code);
    }

    /**
     * 查询全部参数
     * @return
     */
    public List<SysParams> queryAll() {
        return this.queryByCache("SELECT * FROM bb_sys_params p WHERE 1=1 ORDER BY p.`group`");
    }

    public List<SysParams> queryByGroup(String group) {
        return this.queryByCache("select * from bb_sys_params p where p.`group`=?",group);
    }
}
