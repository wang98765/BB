/**
 * Copyright (c) 2013-2016, Jieven. All rights reserved.
 *
 * Licensed under the GPL license: http://www.gnu.org/licenses/gpl.txt
 * To use it on other terms please contact us at 1623736450@qq.com
 */
package com.eova.model;

import java.util.Date;
import java.util.List;

import cn.hutool.core.util.StrUtil;
import com.alibaba.druid.util.StringUtils;
import com.eova.common.base.BaseModel;
import com.eova.common.utils.AddressUtils;
import com.eova.common.utils.net.IpUtil;
import com.eova.common.utils.web.RequestUtil;
import com.eova.common.utils.xx;
import com.eova.config.EovaConst;
import com.jfinal.core.Controller;

import cn.hutool.core.date.DateUtil;

/**
 * 系统操作日志
 *
 * @author Jieven
 * @date 2014-9-10
 */
public class EovaLog extends BaseModel<EovaLog> {

	private static final long serialVersionUID = -1592533967096109392L;

	public static final EovaLog dao = new EovaLog();

	/** 新增 **/
	public static final int ADD = 1;
	/** 修改 **/
	public static final int UPDATE = 2;
	/** 删除 **/
	public static final int DELETE = 3;
	/** 导入 **/
	public static final int IMPORT = 4;
	/** 登陆 **/
	public static final int LOGIN = 5;
	/** 登出 **/
	public static final int LOGOUT = 6;
	
	/**
	 * 操作日志
	 * @param con
	 * @param info 日志详情
	 */
	public void info(Controller con, int type, String info,String remark){
		EovaLog el = new EovaLog();
		// TYPE
		el.set("type", type);
		// UID
		User user = con.getAttr(EovaConst.USER);
		if(user != null)//用户可能未登陆的操作
			el.set("user_id", user.getInt("id"));
		// IP
		String ip = RequestUtil.getIp(con.getRequest());
		el.set("ip", ip);
		el.set("info", info);
		el.set("remark", remark);

		if (!StringUtils.isEmpty(ip)) {

			if(IpUtil.ipIsInner(ip)){
				String serverIp = xx.getConfig("BB_IP_URL");
				//从网络获取本机外网IP
				String netIP = AddressUtils.localNetIpByBB(serverIp);
				if(StrUtil.isEmpty(netIP)){
					el.set("city", "本地");
				}else{
					AddressUtils.IpInfo ipinfo = AddressUtils.getCityInfo(3,netIP);
					el.set("city", ipinfo.getCountry()+"-"+ipinfo.getProvince()+"-"+ipinfo.getCity());

					el.set("remark","外网IP:" + netIP);
				}
			}else{
				AddressUtils.IpInfo ipinfo = AddressUtils.getCityInfo(3,ip);
				el.set("city", ipinfo.getCountry()+"-"+ipinfo.getProvince()+"-"+ipinfo.getCity());
			}

		}

		el.save();
	}
	
	
	
	public List<EovaLog> getUnInitLogs() {
		Date before = DateUtil.offsetDay(new Date(), -7);
		return this.find("select * from eova_log where  create_time>? and city is null order by id desc",before);
	}
	

}