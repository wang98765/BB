/**
 * Copyright (c) 2013-2016, Jieven. All rights reserved.
 *
 * Licensed under the GPL license: http://www.gnu.org/licenses/gpl.txt
 * To use it on other terms please contact us at 1623736450@qq.com
 */
package com.eova.service;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import cn.dev33.satoken.session.Menus;
import cn.dev33.satoken.stp.StpUtil;
import com.eova.common.base.BaseCache;
import com.eova.common.base.BaseService;
import com.eova.common.utils.xx;
import com.eova.common.utils.data.CloneUtil;
import com.eova.common.utils.db.DbUtil;
import com.eova.config.EovaConst;
import com.eova.model.Button;
import com.eova.model.Menu;
import com.eova.model.User;
import com.eova.widget.WidgetUtil;
import com.eova.widget.tree.TreeUtil;
import com.google.common.collect.Lists;
import com.jfinal.plugin.activerecord.Db;

/**
 * 权限数据访问
 * 
 * @author Jieven
 * 
 */
public class AuthService extends BaseService {

	/**
	 * 根据角色ID获取已授权查询的菜单Code
	 * 
	 * @param rid 角色ID
	 * @return
	 */
	public List<String> queryMenuCodeByRid(List<String> rids) {
		String sql = "select DISTINCT(b.menu_code) from eova_role_btn rf LEFT JOIN eova_button b on rf.bid = b.id where b.ui = 'query' and rf.rid in"+DbUtil.joinIds(rids);
		return Db.use(xx.DS_EOVA).query(sql);
	}

	/**
	 * 查询某棵树下是否存在已授权的功能
	 * 
	 * @param parentId 父节点ID 
	 * @param rid　角色ID
	 * @return
	 */
//	public boolean isExistsAuthByPidRid(int parentId, int rid) {
//		// 根据角色ID获取已授权查询的菜单Code
//		List<String> menuCodes = queryMenuCodeByRid(rid);
//		LinkedHashMap<Integer, Menu> result = (LinkedHashMap<Integer, Menu>) getByParentId(parentId);
//		for (String menuCode : menuCodes) {
//			for (Map.Entry<Integer, Menu> map : result.entrySet()) {
//				if (map.getValue().getStr("code").equals(menuCode)) {
//					return true;
//				}
//			}
//		}
//		return false;
//	}

	/**
	 * 递归获取所有父子数据
	 * 
	 * @param parentId 父节点ID
	 * @return List
	 */
	public List<Menu> queryMenuByParentId(int parentId) {
		LinkedHashMap<Integer, Menu> result = (LinkedHashMap<Integer, Menu>) getByParentId(parentId);
		List<Menu> menus = new ArrayList<Menu>();
		for (Map.Entry<Integer, Menu> map : result.entrySet()) {
			menus.add(map.getValue());
		}
		return menus;
	}

	/**
	 * 递归获取所有父子数据
	 * @param parentId 父节点
	 * @return Map
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, Menu> getByParentId(int parentId) {
		LinkedHashMap<Integer, Menu> temp = null;


		// 获取所有菜单信息
		String sql = "select * from eova_menu where is_hide = 0 order by parent_id,order_num";
		List<Menu> records = Menu.dao.queryByCache(sql);

			

			temp = WidgetUtil.menusToLinkedMap(records);

	
			
		

		// 获取某节点下所有数据
		LinkedHashMap<Integer, Menu> result = new LinkedHashMap<Integer, Menu>();
		// 递归获取子节点
		getChildren(temp, parentId, result);

		return CloneUtil.clone(result);
	}


	/**
	 * 递归查找子节点
	 * 
	 * @param all 所有菜单
	 * @param parentId 父节点ID
	 * @param result 节点下所有数据(包括父)
	 */
	private void getChildren(Map<Integer, Menu> all, int parentId, Map<Integer, Menu> result) {
		for (Map.Entry<Integer, Menu> map : all.entrySet()) {
			// 获取父节点
			if (map.getKey() == parentId) {
				result.put(map.getKey(), map.getValue());
			}
			// 获取子节点
			if (map.getValue().getInt("parent_id") == parentId) {
				result.put(map.getKey(), map.getValue());
				// 子ID递归找孙节点
				getChildren(all, map.getKey(), result);
			}
		}
	}

	public void doLoginInit(User user, Consumer<User> loginInit) throws Exception{
		loginInit( user);
		if(loginInit != null)
			loginInit.accept(user);

		user.init();
	}

	public void doLoginInitSession(User user) {
		StpUtil.getSession().setByDevice(EovaConst.USER, user);
		List<Menus>  menus = menuList(null,user).stream().map(Menu::toMenus).collect(Collectors.toList());
		StpUtil.getSession().setCurMenus(menus,null);
	}

	public void doLoginInitWithSession(User user, Consumer<User> loginInit) throws Exception{
		doLoginInit(user,loginInit);

		doLoginInitSession(user);
	}

	/**
	 * 已授权（本地）菜单
	 */
	public List<Menu> menuList(String parentId,User user)  {
		// 获取所有菜单
		List<Menu> menus = Menu.dao.queryMenu();
		// 获取已授权菜单ID
		List<Long> ids = Menu.dao.queryMenuIdByRid(user.getRids());

		// 递归查找已授权功能的上级节点
		HashSet<Long> authPid = new HashSet<Long>();
		for (Long id : ids) {
			Menu m = getParent(menus, id);
			findParent(authPid, menus, m);
		}

		Iterator<Menu> it = menus.iterator();
		while (it.hasNext()) {
			Menu m = it.next();
			//if("首页设计".equals(m.getStr("name"))){
			//	System.out.println(m.getStr("name"));
			//}
			// 构建ztree link属性
			m.put("link", m.getUrl());
			m.remove("url");

			if(!xx.isEmpty(parentId) && !parentId.equals(m.get("parent_id").toString()))
				it.remove();

			long mid = m.getNumber("id").longValue();

			// 已授权目录
			if (authPid.contains(mid))
				continue;

			// 移除未授权菜单
			if (!ids.contains(mid))
				it.remove();


		}

		//处理下，变成 父子结构
		menus = TreeUtil.toTreeList(menus);
		return menus;
	}

	/**
	 * 全部的菜单（保护跨系统的）
	 * @return
	 */
	public List<Menus> menuListAll(String parentId,User user){
		List<Menus> localMenus = menuList(parentId,user).stream().map(Menu::toMenus).collect(Collectors.toList());

		if("1".equalsIgnoreCase(xx.getConfig("authority_other_show","0"))){
			List<Menus> menus = StpUtil.getSession().getAllMenus(true);//.set(AuthCloud.getEovaApp().getId()+"_menus",menus);MENUS_
			List<Menus> result = Lists.newArrayListWithExpectedSize(localMenus.size()+menus.size());
			result.addAll(localMenus);

			result.addAll(menus);
			return result;
		}else
			return localMenus;
	}

	/**
	 * 初始权限（根据角色）
	 *
	 * @param user
	 * @throws Exception
	 */
	protected void loginInit( User user) throws Exception {
		// 初始化获取授权信息

		Set<String> auths = new HashSet<String>();
//		String sql = "SELECT bs FROM eova_role_btn rf LEFT JOIN eova_button b ON rf.bid = b.id WHERE rf.rid in "+DbUtil.joinIds(user.getRids());
//		List<Record> bss = Db.use(xx.DS_EOVA).find(sql);


		if(!xx.isEmpty(user.getRids())) {
			List<Button> bts = Button.dao.roleButtons(user.getRids());
			for (Button b : bts) {
				String bs = b.getStr("bs");
				if (xx.isEmpty(bs)) {
					continue;
				}
				if (!bs.contains(";")) {
					auths.add(bs);
					continue;
				}
				String[] strs = bs.split(";");
				for (String str : strs) {
					auths.add(str);
				}
			}
			/*if (xx.isEmpty(auths)) {
				throw new Exception("用户角色没有任何授权,请联系管理员授权");
			}*/
		}
		user.put("auths", auths);

		// 子类可重写添加业务属性和对象
	}



	/**
	 * 获取父菜单
	 *
	 * @param menus
	 * @param id
	 * @return
	 */
	private  Menu getParent(List<Menu> menus, Long id) {
		Optional<Menu> mopt = menus.stream().filter(m->m.getNumber("id").longValue() == id).findFirst();
		return mopt.orElse(null);
	}

	/**
	 *
	 * 递归向上查找父节点
	 *
	 * @param authPid 找到的父节点
	 * @param menus 所有菜单
	 */
	private void findParent(HashSet<Long> authPid, List<Menu> menus, Menu m) {
		if (m == null) {
			return;
		}
		long pid = m.getNumber("parent_id").longValue();
		if (pid == 0) {
			return;
		}
		authPid.add(pid);

		Menu p = getParent(menus, pid);
		findParent(authPid, menus, p);
	}
}