package com.eova.widget.captcha;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.QuadCurve2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;

import com.eova.common.utils.xx;
import com.jfinal.captcha.Captcha;
import com.jfinal.captcha.CaptchaManager;
import com.jfinal.captcha.CaptchaRender;
import com.jfinal.captcha.ICaptchaCache;
import com.jfinal.core.Controller;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.StrKit;
import com.jfinal.render.Render;
import com.jfinal.render.RenderException;

/**
 * 原始版本存在验证成功才清除验证码，现调整成只要用过即清除
 * @author Administrator
 *
 */
public class CaptchaIntimeCleanRender extends com.jfinal.captcha.CaptchaRender {

	/**
	 * 校验用户输入的验证码是否正确
	 * @param controller 控制器
	 * @param userInputString 用户输入的字符串
	 * @return 验证通过返回 true, 否则返回 false
	 */
	public static boolean validate(Controller controller, String userInputString) {
		try {
			String captchaKey = controller.getCookie(captchaName);
			if (validate(captchaKey, userInputString)) {
				return true;
			}
			return false;
		}finally{
			controller.removeCookie(captchaName);
		}
	}
	
	/**
	 * 校验用户输入的验证码是否正确
	 * @param captchaKey 验证码 key，在不支持 cookie 的情况下可通过传参给服务端
	 * @param userInputString 用户输入的字符串
	 * @return 验证通过返回 true, 否则返回 false
	 */
	public static boolean validate(String captchaKey, String userInputString) {
		ICaptchaCache captchaCache = CaptchaManager.me().getCaptchaCache();
		Captcha captcha = captchaCache.get(captchaKey);
		try {
			if (captcha != null && captcha.notExpired() && captcha.getValue().equalsIgnoreCase(userInputString)) {
				
				return true;
			}
			return false;
		}finally{
			if(!xx.isEmpty(captchaKey))
				captchaCache.remove(captcha.getKey());
		}
	}

}
