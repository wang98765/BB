package com.eova.ai.vo;

import com.eova.model.AiChatSession;
import com.eova.model.AiModel;
import com.eova.model.AiRobot;
import com.jfinal.plugin.activerecord.IRow;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * <p>Project: bb-project - ChatSession</p>
 * <p>描述：ChatSession-存入redis</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/6 15:27
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ChatSession implements Serializable {
    private static final long serialVersionUID = -990334519496260591L;

    public static final String CHAT_KEY = "AI_CHAT_SESSION";

    /**
     * 会话Id，和aiChatSession.getLong("id")一致
     */
    private Long sessionId;

    /**
     * 会话具体数据
     */
    private AiChatSession aiChatSession;

    /**
     * 会话关联的机器人
     */
    private AiRobot robot;

    /**
     * 模型向量库名(无则不需要本地向量库会话了)
     */
    private String collectionName ;

    /**
     * 机器人关联的本底值知识库模型
     */
    private List<AiModel> rebotModels;


}
