package com.eova.ai.interceptor;

import com.alibaba.fastjson.JSON;
import com.eova.aop.AopContext;
import com.eova.aop.MetaObjectIntercept;
import com.eova.common.utils.jfinal.RecordUtil;
import com.eova.common.utils.xx;
import com.eova.model.BbFiles;
import com.eova.model.MetaObject;
import com.jfinal.kit.LogKit;
import com.jfinal.plugin.activerecord.Record;
import com.oss.model.Address;
import com.oss.model.Orders;
import com.oss.model.Users;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.List;

/**
 * <p>Project: bb-project - AiModelInterceptor</p>
 * <p>描述：AiModle 拦截业务</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/6 22:19
 * @Version 1.0
 * @since 8
 */
@Slf4j
public class AiModelIntercept extends MetaObjectIntercept {

    public String addBefore(AopContext ac) throws Exception {
        Record data = ac.record;
        initUploadFile(data);

        return super.addBefore(ac);
    }


    public String updateBefore(AopContext ac) throws Exception {
        Record data = ac.record;
        initUploadFile(data);

        return super.updateBefore(ac);
    }

    public String addSucceed(AopContext ac) throws Exception {
        List<Record> recordList = ac.records;
        afterSaveData(recordList);
        return super.addSucceed(ac);
    }

    public String updateSucceed(AopContext ac) throws Exception {
        List<Record> recordList = ac.records;
        afterSaveData(recordList);
        return null;
    }


    private void initUploadFile(Record data){
        String addType = data.getStr("add_type");//1=录入问答，2=提交文件

        if("2".equals(addType)){
            String uploadFile = data.getStr("upload_file");
            BbFiles bbFiles = BbFiles.dao.getByUrl(uploadFile);
            String forName = bbFiles.getStr("form_name");
            String name = bbFiles.getStr("name");
            data.set("file_name",name);
            data.set("file_type",forName);
            data.set("oss_id",bbFiles.getLong("id"));
        }
        data.set("state",0);
    }

    private void afterSaveData(List<Record> recordList){

        log.info("保存数据完成，启动异步训练:{}", JSON.toJSONString(recordList));
    }
}
