package com.eova.ai.handler.impl;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.eova.ai.DocParser.AbstractParser;
import com.eova.ai.enums.AiModelTrainType;
import com.eova.ai.handler.ModeTrainHandler;
import com.eova.model.AiModel;
import com.eova.model.AiRobot;
import com.eova.model.BbFiles;
import com.eova.service.sm;
import com.google.common.base.Joiner;

import cn.bblocks.chatgpt.ChatGPT;
import cn.bblocks.chatgpt.ChatGPTStream;
import cn.bblocks.chatgpt.EmbeddingModel;
import cn.bblocks.chatgpt.entity.chat.DataSqlEntity;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.List;
import java.util.concurrent.Executor;

@Slf4j
public class FileModeTrainHandler implements ModeTrainHandler {

    @Resource
    Executor aiExecutor;

    public Integer getTrainType(){
        return AiModelTrainType.FILE.getType();
    }

    /**
     * 训练
     * @param aiModel
     * @return 1=成功，0=失败
     */
    public int train(AiModel aiModel, AiRobot robot){
        Long ossId = aiModel.getLong("oss_id");
        BbFiles sysOss = BbFiles.dao.getByCache(ossId);

        String content = null;

        ChatGPT chatGpt = sm.chatService.getTheGpt(robot);
        ChatGPTStream gptStream = sm.chatService.getTheStreamGpt(chatGpt);
        EmbeddingModel embeddingModel = sm.chatService.getTheEmbeddingModel(chatGpt,gptStream);
        try {
            AbstractParser parser = AbstractParser.getParserName(sysOss.getStr("form_name"));
            List<String> sentence = null;
           if(parser != null){
               sentence = readFile(sysOss,parser);
               content = Joiner.on(AbstractParser.PART_SPLITTER).join(sentence);
               aiModel.set("content",content);
           }else{
               log.warn("训练模型：{} 不支持的格式：{}",aiModel.getLong("id"),sysOss.getStr("form_name"));
               aiModel.set("remark","不支持的格式："+sysOss.getStr("form_name"));
               return 0;
           }
            DataSqlEntity params = new DataSqlEntity();
            params.setModelId(aiModel.getLong("id").intValue());
            params.setRobotId(robot.getLong("id").intValue());
            embeddingModel.saveModel(sentence,null,params);

        }catch (IOException e){
            log.error("文件读取异常：{} 异常：",aiModel.getLong("id"),e);
            aiModel.set("remark","文件读取异常！");
            return 0;
        }catch (Exception e){
            log.error("训练模型：{} 异常：",aiModel.getLong("id"),e);
            aiModel.set("remark",StrUtil.maxLength(e.getMessage(),40));
            return 0;
        }
        return 1;
    }

    public List<String> readFile(@NonNull BbFiles sysOss,@NonNull AbstractParser parser) throws IOException{
            PipedOutputStream out = null;
            PipedInputStream in = null;
            try {
                out = new PipedOutputStream();
                in = new PipedInputStream(out);

                final PipedOutputStream fout = out;
                Runnable runnable = ()->{
                    long ret = HttpUtil.download(sysOss.getStr("url"), fout, true);
                };
                ThreadUtil.execAsync(runnable,true);

                List<String> sentence = parser.parse(in);

                return sentence;
            }finally {
                if (in != null)
                    in.close();
            }
    }

    public String readPdf(String url) throws IOException{
        PipedOutputStream out = null;
        PipedInputStream in = null;
        try {
            out = new PipedOutputStream();
            in = new PipedInputStream(out);

            final PipedOutputStream fout = out;
            aiExecutor.execute( () -> {
                long ret = HttpUtil.download(url, fout, true);
                });

            PDDocument document = PDDocument.load(in);
            PDFTextStripper pdfStripper = new PDFTextStripper();
            return pdfStripper.getText(document);
        /*}catch (Exception e) {
            log.error("文件读取异 异常：",e);
            return null;*/
        }finally {
            if(out != null)
                out.close();

            if (in != null)
                in.close();
        }
    }


    public static void main(String[] atgs) throws IOException{
        FileModeTrainHandler h = new FileModeTrainHandler();
       String text = h.readPdf("https://img-ai.9jodia.net/aimodel/20230904/03ee8f55ad9f4b139bf1512dbe7e1b63.pdf");

        System.out.println("===========>");
        System.out.println(text);
    }

}
