package com.eova.ai.handler.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.eova.ai.DocParser.AbstractParser;
import com.eova.ai.enums.AiModelTrainType;
import com.eova.ai.handler.ModeTrainHandler;
import com.eova.model.AiModel;
import com.eova.model.AiRobot;
import com.eova.service.sm;
import com.google.common.base.Splitter;
import cn.bblocks.chatgpt.ChatGPT;
import cn.bblocks.chatgpt.ChatGPTStream;
import cn.bblocks.chatgpt.EmbeddingModel;
import cn.bblocks.chatgpt.entity.chat.DataSqlEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

@Slf4j
public class ContentTrainHandler implements ModeTrainHandler {


    public Integer getTrainType(){
        return AiModelTrainType.CONTENT.getType();
    }

    /**
     * 训练
     * @param aiModel
     * @return 1=成功，0=失败
     */
    @Override
    public int train(AiModel aiModel, AiRobot robot){
        String content = aiModel.getStr("content");

        ChatGPT chatGpt = sm.chatService.getTheGpt(robot);
        ChatGPTStream gptStream = sm.chatService.getTheStreamGpt(chatGpt);
        EmbeddingModel embeddingModel = sm.chatService.getTheEmbeddingModel(chatGpt,gptStream);
        try {
            DataSqlEntity params = new DataSqlEntity();
            params.setModelId(aiModel.getLong("id").intValue());
            params.setRobotId(robot.getLong("id").intValue());

            embeddingModel.saveModel(Splitter.on(AbstractParser.PART_SPLITTER).splitToList(content),null,params);
        }catch (Exception e){
            log.error("训练模型：{} 异常：",aiModel.getLong("id"),e);
            aiModel.set("remark",StrUtil.maxLength(e.getMessage(),40));
            return 0;
        }
        return 1;
    }

    public String readPdf(String url) throws IOException{
        PipedOutputStream out = null;
        PipedInputStream in = null;
        try {
            out = new PipedOutputStream();
            HttpUtil.download(url, out, true);

            in = new PipedInputStream(out);
            PDDocument document = PDDocument.load(in);
            PDFTextStripper pdfStripper = new PDFTextStripper();
            return pdfStripper.getText(document);
//        }catch (IOException e) {
//            log.error("文件读取异常：{} 异常：",aiModel.getId(),e);
//            aiModel.setRemark("文件读取异常！");
//            return 0;
        }finally {
            if(out != null) {
                out.close();
            }

            if (in != null) {
                in.close();
            }
        }
    }

}
