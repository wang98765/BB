package com.eova.ai.DocParser;

import org.reflections.Reflections;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 文档读取
 * @author Zhao
 * @date 2023年09月10日 下午3:23
 * @description
 */
public abstract class AbstractParser {
    /**
     * 全部的文档读取器
     */
    private static Map<String,AbstractParser> parsers;
    static {
        parsers = allParses();
    }
    /**
     * 段分隔（至少空一行的--2个换行）
     */
    public static final String PART_SPLITTER = "\\r\\n\\r\\n";

    /**
     * 换行
     */
    public static final String LINE_SPLITTER = "\\r\\n";

    public abstract String getParserName();

    public abstract List<String> parse(InputStream inputStream) throws IOException;

    /**
     * 根据格式名获取解码器
     * @param name
     * @return
     */
    public static  AbstractParser getParserName(String name){
        return parsers.get(name);
    }

    /**
     * 支持的格式
     * @return
     */
    public static Set<String> getSupports(){
       return parsers.keySet();
    }


    private static Map<String,AbstractParser> allParses(){
        // 获取当前类的Class对象
        Class<?> clazz = AbstractParser.class;
        // 获取当前类的包名
        Package pkg = clazz.getPackage();
        String packageName = pkg.getName();

        Set<Class<? extends AbstractParser>> subTypes = childClass();
        Map<String,AbstractParser> result = new HashMap<String,AbstractParser>();

        for(Class<? extends AbstractParser> one:subTypes){
            try {
                // one.
                //  Class catClass = Class.forName(one.getName());
                // 实例化这个类
                AbstractParser parser = (AbstractParser) one.newInstance();
                result.put(parser.getParserName(), parser);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        return result;
    }

    private static Set<Class<? extends AbstractParser>> childClass(){
        // 获取当前类的Class对象
        Class<?> clazz = AbstractParser.class;
        // 获取当前类的包名
        Package pkg = clazz.getPackage();
        String packageName = pkg.getName();

        Reflections reflections = new Reflections(packageName);
        Set<Class<? extends AbstractParser>> subTypes = reflections.getSubTypesOf(AbstractParser.class);
       // subTypes.forEach(x -> System.out.println(x));
        return subTypes;
    }

}
