package com.eova.ai.DocParser;

import cn.hutool.core.util.StrUtil;
import com.google.common.base.Joiner;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * txt文件读取，以一个空行为段分隔点
 * @author Zhao
 * @date 2023年09月10日 下午3:23
 * @description
 */
public class TxtParse extends AbstractParser {


    public String getParserName(){
        return "txt";
    }

    @Override
    public List<String> parse(InputStream inputStream) throws IOException {
        List<String> tempList = new ArrayList<>();
        List<String> ans = new ArrayList<>();

        try( // 将字节流向字符流转换
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
             // 创建字符流缓冲区
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader);)
        {

            String line;
            // 按行读取
            while ((line = bufferedReader.readLine()) != null) {
                line = StrUtil.trim(line);
                if (StrUtil.isEmpty(line)){//分隔段出现了
                    if(!tempList.isEmpty()){
                        String  part = Joiner.on(AbstractParser.LINE_SPLITTER).join(tempList);
                        ans.add(part);
                        tempList.clear();
                    }
                }else{//完整段，直接放入零时列表
                    tempList.add(line);
                }
            }

            if(!tempList.isEmpty()){
                String  part = Joiner.on(AbstractParser.LINE_SPLITTER).join(tempList);
                ans.add(part);
                tempList.clear();
            }
        }finally {
            inputStream.close();
        }

        return ans;
    }

    public static void main(String[] args) throws Exception {
        AbstractParser txtParse = AbstractParser.getParserName("txt");



        File file = new File("d:\\2222.txt");
       List<String> result = txtParse.parse(new FileInputStream(file));

        for (String line : result) {
            System.out.println(line);
        }


        AbstractParser.getSupports();

    }
}
