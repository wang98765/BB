package com.eova.ai.listener;

import com.eova.ai.AiChatWebSocket;
import cn.bblocks.chatgpt.entity.chat.Message;
import cn.bblocks.chatgpt.listener.AbstractStreamListener;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * <p>Project: bb-project - WebSocketStreamListener</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/8 15:22
 * @Version 1.0
 * @since 8
 */
@Slf4j
public class WebSocketStreamListener extends AbstractStreamListener {

    private String token;
    public WebSocketStreamListener(String token){
        this.token = token;
    }
    public void onMsg(Object message) {
        //System.out.print(message);
        try{
            AiChatWebSocket.OnMessage(token,(Message) message);
        }catch (IOException e){
            log.warn("推送消息至：{}（ws）异常！",token);
        }

    }

    public void onError(Throwable throwable, String response) {

    }
}
