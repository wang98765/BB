/**
 * 
 */
package com.eova.ai;

import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.fastjson.JSON;
import com.eova.common.utils.xx;
import com.eova.config.EovaConst;
import com.eova.model.User;
import com.jfinal.handler.Handler;
import com.jfinal.kit.StrKit;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

/**
 * @Description: WebSocketHandler
 * @author Jin
 * @date 2020-12-3117:12:43
 * @version v1.0
 */
@Slf4j
public class AiChatWebSocketHandler extends Handler {
    private Pattern filterUrlRegxPattern;

    public AiChatWebSocketHandler(String filterUrlRegx) {
        if (StrKit.isBlank(filterUrlRegx))
            throw new IllegalArgumentException("The para filterUrlRegx can not be blank.");
        filterUrlRegxPattern = Pattern.compile(filterUrlRegx);
    }
    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        if (filterUrlRegxPattern.matcher(target).find()) {
            String token = request.getHeader(xx.getConfig("sa-token.token-name","bb_token"));
            request.setAttribute("token",token);


            log.info("token:{}",token);
            return;
        }else {
            /*User user =(User) StpUtil.getSession().getByDevice(EovaConst.USER);
            request.setAttribute(EovaConst.USER,user);
            log.info("User: " , JSON.toJSONString(user));*/
            next.handle(target, request, response, isHandled);
        }

    }

}
