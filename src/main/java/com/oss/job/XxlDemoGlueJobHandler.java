package com.oss.job;

import com.xxl.job.core.handler.IJobHandler;

import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import com.eova.common.utils.xx;
import com.jfinal.plugin.activerecord.Db;

//本处不生效，写完代码直接copy至xxl job GLUE(java)即可（注意@Slf4j不生效）
//@Slf4j
public class XxlDemoGlueJobHandler extends IJobHandler {

	@Override
	public void execute() throws Exception {
		XxlJobHelper.log("XXL-JOB, Hello World.");
      
      //log.info("DemoGlueJobHandler执行一次1");
      System.out.println("DemoGlueJobHandler执行一次2");
      
      	List allCode = Db.use(xx.DS_MAIN).query("SELECT * FROM dicts limit 1");
      	System.out.println("size:"+allCode.size());
	}

}
